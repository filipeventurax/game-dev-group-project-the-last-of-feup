# THE LAST OF FEUP

This group project was developed for FEUP's subject of Digital Games Development, using Godot, by Diogo Filipe Martins, Matheus Santos, Ricardo Nunes.

# Where To Play It 

[This game can be played in here.](https://g2-equipa.itch.io/the-last-of-feup)

# Video Trailer of the Game

[The video trailer of the game can be seen in here.](https://youtu.be/Vk7XgK9dGe4?si=SFu9Xk6A1CJ5XHXj)

# Synopsis

Suddenly, everyone at FEUP became a zombie and you are the only one who can defeat them! For that, you were gifted with a superpower: the ability to shoot portals that allows you to teleport yourself!

Find the antidote in the Chemical Engineering Department, before the army terminates everyone, and save the university!


## Commands:
* W: Jump
* A: Move Left
* D: Move Right
* E: Grab/ Ungrab Computers and Turn On/Off Switches
* S/Right click: Create blue portal
* Left click: Create orange portal

## Rules:
* You can only shoot portals against surfaces;
* You can shoot 2 types of portals: blue and orange ones. If you enter in a blue one, you exit in a blue one, and if you enter in a orange one, you exit in a orange one;
* You will need to press switches or to hold the computer and drop it on buttons in order to achieve the goal of the character:
** Computers can be grabbed and ungrabbed but not dragged;
** The buttons on the floor work when the player or the computer are on top of them;
** Switches may allow the use of elevators or deflect temporarily walls or parts of the floor that are obstacles.
* You can use the elevators but be careful with the holes on the first floor;
* You have two types of enemies: 
** Zombies: They are teachers or students and they are melee (they need to be close to you to kill you). One attack from them will kill you;
** Soldiers: They are from the army and they are ranged (they can kill you from far away and with a single attack). Their goal is to kill the zombies but if you are on their way, they assume you are one of them.
* You finish the game when you get the potion!


# Group Members:

* Diogo Filipe Martins, up201806280
* Matheus Santos, up202111214
* Ricardo Nunes, up201706860
